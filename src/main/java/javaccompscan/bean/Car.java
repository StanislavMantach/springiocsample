package javaccompscan.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class Car {

    private final Engine engine;
    private final Transmission transmission;

    @PostConstruct
    public void init(){
        System.out.println("here we are in the post construct method");
    }

    @Autowired
    public Car(Engine engine, Transmission transmission) {
        this.engine = engine;
        this.transmission = transmission;
    }

    @Override
    public String toString()
    {
        return String.format("Engine: %s Transmission: %s", engine, transmission);
    }
}