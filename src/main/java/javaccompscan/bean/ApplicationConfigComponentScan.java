package javaccompscan.bean;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by smanta on 21.02.2018.
 */
@Configuration
@ComponentScan("javaccompscan.bean")
public class ApplicationConfigComponentScan {

}
