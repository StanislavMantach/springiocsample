package javaccompscan.bean;

import org.springframework.stereotype.Component;

@Component
public class Transmission {
	private String type = "automatic";

	@Override
	public String toString() {
		return String.format("%s", type);
	}
}
