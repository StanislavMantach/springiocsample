package javaccompscan.bean;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by smanta on 19.02.2018.
 */
public class JavacScanContextDemo {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfigComponentScan.class);
        Car car = context.getBean(Car.class);
        System.out.println(car.toString());
        Object engine = context.getBean("engine");
        System.out.println(engine);
    }
}
