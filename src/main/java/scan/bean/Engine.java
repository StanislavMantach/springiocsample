package scan.bean;

import org.springframework.stereotype.Component;

@Component
public class Engine {

    private String type = "v4";
    private int volume = 2;

    @Override
    public String toString() {
        return String.format("%s %d", type, volume);
    }
}
