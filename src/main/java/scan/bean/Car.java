package scan.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class Car {

    @Autowired
    private Engine engine;

    @Autowired
    private Transmission transmission;

    @PostConstruct
    public void init(){
        System.out.println("here we are in the post construct method");
    }

    @Override
    public String toString()
    {
        return String.format("Engine: %s Transmission: %s", engine, transmission);
    }
}
