package simple.bean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by smanta on 19.02.2018.
 */
public class SimpleContextDemo {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("simpleXmlContext.xml");
        Car car = context.getBean(Car.class);
        System.out.println(car.toString());

    }
}
