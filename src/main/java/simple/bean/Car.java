package simple.bean;

public class Car {

    private Engine engine;
    private Transmission transmission;

    @Override
    public String toString() {
        return String.format("Engine: %s Transmission: %s", engine, transmission);
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }
}
