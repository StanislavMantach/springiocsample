package javaconfigxml.bean;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by smanta on 21.02.2018.
 */
@Configuration
@ImportResource("simpleJavaXmlContext.xml")
public class ApplicationConfigXml {

}
