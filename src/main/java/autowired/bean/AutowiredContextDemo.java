package autowired.bean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by smanta on 19.02.2018.
 */
public class AutowiredContextDemo {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("autowireXmlContext.xml");
        Car car = context.getBean(Car.class);
        System.out.println(car.toString());
        Object engine = context.getBean("engine");
        System.out.println(engine);
    }
}
