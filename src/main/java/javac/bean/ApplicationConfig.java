package javac.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by smanta on 21.02.2018.
 */
@Configuration
public class ApplicationConfig {

    @Bean
    public Transmission getTransmission(){
        return new Transmission("automatic");
    }

    @Bean("engine")
    public Engine getEngine(){
        return new Engine("V4", 2);
    }

    @Bean
    public Car getCar(){
        return new Car(getEngine(), getTransmission());
    }

}
